LATEXMK=latexmk
LATEXMAKEINDEX=makeindex
LATEXMK_OPTIONS= -bibtex -pdf -pdflatex="pdflatex -interaction=nonstopmode"
LATEXMAKEINDEX_OPTIONS= Thesis.nlo -s nomencl.ist -o Thesis.nls

pdf: Thesis.tex
	@echo Input file: $<
	$(LATEXMK) $(LATEXMK_OPTIONS) $<
	$(LATEXMAKEINDEX) $(LATEXMAKEINDEX_OPTIONS)
	$(LATEXMK) $<

clean:
	-$(LATEXMK) -bibtex -C Thesis
